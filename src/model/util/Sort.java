package model.util;
import model.util.*;

public class Sort {
	
	private static Comparable[] aux; 
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {
		
		// TODO implementar el algoritmo ShellSort
		
		 int n = datos.length;

	        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
	        int h = 1;
	        while (h < n/3) h = 3*h + 1; 

	        while (h >= 1) {
	            // h-sort the array
	            for (int i = h; i < n; i++) {
	                for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) {
	                    exchange(datos, j, j-h);
	                }
	            }
	            assert isHsorted(datos, h); 
	            h /= 3;
	        }
	        assert isSorted(datos);
	}
	
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo MergeSort
		aux = new Comparable[datos.length];
		sortM(datos, 0, datos.length - 1);
		
	}
	private static void sortM(Comparable[] a, int lo, int hi) {
		if (hi <= lo) return; // Sort a[lo..hi].
		int mid = lo + (hi - lo)/2;
		sortM(a, lo, mid);
		// Sort left half.
		sortM(a, mid+1, hi);
		// Sort right half.
		mergeSort(a, lo, mid, hi);
		} 
	public static void mergeSort(Comparable[] a, int lo, int mid, int hi)
	{
	// Merge a[lo..mid] with a[mid+1..hi].
	int i = lo, j = mid+1;
	for (int k = lo; k <= hi; k++)
	// Copy a[lo..hi] to aux[lo..hi].
	aux[k] = a[k];
	for (int k = lo; k <= hi; k++)
	// Merge back to a[lo..hi].
	if (i > mid) a[k] = aux[j++];
	else if (j > hi ) a[k] = aux[i++];
	else if (less(aux[j], aux[i])) a[k] = aux[j++];
	else a[k] = aux[i++];
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo QuickSort
		StdRandom stdRandom= new StdRandom();
		StdRandom.shuffle(datos); // Eliminate dependence on input.
		sortQ(datos, 0, datos.length - 1);
	}
	 // quicksort the subarray from a[lo] to a[hi]
    private static void sortQ(Comparable[] a, int lo, int hi) { 
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sortQ(a, lo, j-1);
        sortQ(a, j+1, hi);
    }

    // partition the subarray a[lo..hi] so that a[lo..j-1] <= a[j] <= a[j+1..hi]
    // and return the index j.
    private static int partition(Comparable[] a, int lo, int hi) {
        int i = lo;
        int j = hi + 1;
        Comparable v = a[lo];
        while (true) { 

            // find item on lo to swap
            while (less(a[++i], v)) {
                if (i == hi) break;
            }

            // find item on hi to swap
            while (less(v, a[--j])) {
                if (j == lo) break;      // redundant since a[lo] acts as sentinel
            }

            // check if pointers cross
            if (i >= j) break;

            exchange(a, i, j);
        }

        // put partitioning item v at a[j]
        exchange(a, lo, j);

        // now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
        return j;
    }
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable<Comparable<?>> v, Comparable<?> w)
	{
		// TODO implementar
		return v.compareTo(w) < 0;
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Object swap = datos[i];
        datos[i] = datos[j];
        datos[j] = (Comparable<?>) swap;
	}

	private static boolean isSorted(Comparable[] a) {
        for (int i = 1; i < a.length; i++)
            if (less(a[i], a[i-1])) return false;
        return true;
    }
	
	 private static boolean isHsorted(Comparable[] a, int h) {
	        for (int i = h; i < a.length; i++)
	            if (less(a[i], a[i-h])) return false;
	        return true;
	    }
}
