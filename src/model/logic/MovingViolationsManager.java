package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.Pila;
import model.vo.VOMovingViolation;

public class MovingViolationsManager {

	private Cola<VOMovingViolation> pCola = new Cola<>();
	private Pila<VOMovingViolation> pPila = new Pila<>();
	private int cont;
	Comparable<VOMovingViolation> [ ] muestra;


	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub

		try {
			@SuppressWarnings("resource")
			CSVReader Csvr = new CSVReader(new FileReader(movingViolationsFile));
			String[] lector = new String[16];


			while((lector = Csvr.readNext()) != null)
			{
				lector = Csvr.readNext();

				if(lector != null)
				{

					SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
					String[] x = lector[13].split("T");
					String z = x[0];
				
					Date fecha = null;
					try {
						fecha = formatoDelTexto.parse(z);
					} catch (ParseException ex) {
						ex.printStackTrace();
					}
					System.out.println(fecha.toString());

					VOMovingViolation infraccion = new VOMovingViolation(Integer.parseInt(lector[0]), lector[2], Integer.parseInt(lector[9]),  fecha , lector[12], lector[15], lector[14]);
					pCola.enqueue(infraccion);
					pPila.push(infraccion);
					System.out.println(lector[0]);	
					cont++;

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public Cola<VOMovingViolation> darCola()
	{
		return pCola;
	}
	public Pila<VOMovingViolation> darPila()
	{
		return pPila;
	}
	public int darNumeroInfracciones()
	{
		return cont;
	}

}
