package model.util;

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;

public class SortTest {

	// Muestra de dates a ordenar
	private Comparable[] arreglo ;
	
	@Before
	public void setUp() {
		arreglo=new Comparable[6];
		arreglo[0]=1;
		arreglo[1]=12;
		arreglo[2]=5;
		arreglo[3]=7;
		arreglo[4]=20;
		arreglo[5]=0;
	}

	@Test
	public void mergeTest()  {
		setUp();
		Sort.ordenarMergeSort(arreglo);
		Comparable[] array={0,1,5,7,12,20};
		assertArrayEquals(array, arreglo);
	}
	@Test
	public void quickTest() {
		setUp();
		Sort.ordenarQuickSort(arreglo);
		Comparable[] array={0,1,5,7,12,20};
		assertArrayEquals(array, arreglo);
	}
	@Test
	public void shellTest() {
		setUp();
		Sort.ordenarShellSort(arreglo);
		Comparable[] array={0,1,5,7,12,20};
		assertArrayEquals(array, arreglo);
	}


}
